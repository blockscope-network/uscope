package main

import (
	"fmt"
	"log"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/tidwall/gjson"
	tb "gopkg.in/tucnak/telebot.v2"
)


type ValidatorStatus struct {
	network string
	height string
	lastBlockTime string
	syncing string
	votingPower string
}

type ValidatorSigning struct {
	jailedUntil string
	tombstoned string
	missedBlocks string
}

type Network struct {
	tx int
	rx int
}

type System struct {
	date string
	users int
	sessions string
	disk string
	mem string
	swap string
	cpu string
	network Network
}


const cli = "umeed"
const node = "tcp://con-umee-val-1:26657"
const chainId = "umee-1"


const botKey = "1834649026:AAHh1f1udPA9-N9Q9rieKVzj7TMt6jE13yI"
// const botChatId = 1252858258

const serviceName string = "umeed.service"
// const validatorChainId string = "chainId"
// const validatorOperatorAddress string = "fetchvaloper17tznsfw0jz2tmkpsqqv40tzmd0nhndf9me4585"
const validatorConsPublicAddress string = "fetchvalconspub1zcjduepq5msvny0c44ul0ntcu5qacahuj907efg6ygpsf7t2n3tlkk8364xsrhpdml"

const networkIface = "enp42s0"
const diskDevId = "nvme0n1p2"

const bytesInAMegabyte = 1048576


var validatorChainId string


func schedule(callback func(), delay time.Duration, quit chan bool) bool {
	go func() {
		for range time.Tick(delay * time.Second) {
			select {
				case <- quit:
					fmt.Println("Schedule :: terminating channel")
					return

				default:
					callback()
			}
		}
	}()

	return true
}


func execCommand(app string, args ...string) string {
	cmd := exec.Command(app, args...)
	stdout, err := cmd.Output()

	if err != nil {
		fmt.Println(err.Error())
		return err.Error()
	}

	return strings.TrimSpace(string(stdout))
}


func getServiceStatus() string {
	command := "bash"
	args := []string{"-c", "systemctl status " + serviceName + " | grep -i \"active:\" | awk '{print $2}' "}
	res := execCommand(command, args...)

	return res
}


func getValidatorStats() ValidatorStatus {
	args := []string{"status", "--node", node}
	res := execCommand(cli, args...)

	var stats ValidatorStatus

	stats.network = gjson.Get(res, "node_info.network").String()
	stats.height = gjson.Get(res, "sync_info.latest_block_height").String()
	stats.lastBlockTime = gjson.Get(res, "sync_info.latest_block_time").String()
	stats.syncing = gjson.Get(res, "sync_info.catching_up").String()
	stats.votingPower = gjson.Get(res, "validator_info.voting_power").String()

	return stats
}


func getValidatorNetwork() string {
	return getValidatorStats().network
}


func getValidatorSigning(validatorChainId string) ValidatorSigning {
	args := []string{"query", "slashing", "signing-info", validatorConsPublicAddress, "--chain-id", validatorChainId}
	res := execCommand(cli, args...)

	var signing ValidatorSigning

	signing.jailedUntil = extractFieldFromText("jailed_until", res)
	signing.tombstoned = extractFieldFromText("tombstoned", res)
	signing.missedBlocks = extractFieldFromText("missed_blocks_counter", res)

	return signing
}


func getSystemStats() System {
	var system System

	system.date = time.Now().Format("2006.01.02 - 15:04:05")
	system.users = getUserSessionsCount()
	system.sessions = getUserSessions()
	system.disk = getDiskStats()
	system.mem = getMemStats()
	system.swap = getSwapStats()
	system.cpu = getCpuStats()
	system.network = getNetworkStats()

	return system
}


func getDiskStats() string {
	command := "bash"
	args := []string{"-c", "df -h /dev/" + diskDevId + " | tail -n 1 | awk '{ print $5 }'"}
	res := execCommand(command, args...)

	return res
}


func getMemStats() string {
	command := "bash"
	usedMemArgs := []string{"-c", "free | grep Mem | awk '{print ($3 + $5)/$2 * 100.0}'"}
	resUsedMem := execCommand(command, usedMemArgs...)

	return resUsedMem + "%"
}


func getSwapStats() string {
	command := "bash"
	usedSwapArgs := []string{"-c", "free | grep Swap | awk '{print $3 / $2 * 100.0}'"}
	resUsedSwap := execCommand(command, usedSwapArgs...)

	return resUsedSwap + "%"
}


func getCpuStats() string {
	const cpuTimeRangeInSecs = "0.1"

	command := "bash"
	args := []string{"-c", "awk '{u=$2+$4; t=$2+$4+$5; if (NR==1){u1=u; t1=t;} else print ($2+$4-u1) * 100 / (t-t1) \"%\"; }' <(grep 'cpu ' /proc/stat) <(sleep " + cpuTimeRangeInSecs + ";grep 'cpu ' /proc/stat)"}
	res := execCommand(command, args...)

	// Top 10 cpu and Top 10 mem processes
	// ps -eo pcpu,pid,user,command | sort -k 1 -r | head -10
	// ps -eo pmem,pid,user,command | sort -k 1 -r | head -10

	return res
}


func getNetworkStats() Network {
	var network Network
	var res string

	command := "cat"
	basePath := "/sys/class/net/" + networkIface + "/statistics/"

	argsTx := []string{basePath + "tx_bytes"}
	res = execCommand(command, argsTx...)
	tx, txErr := strconv.Atoi(res)

	argsRx := []string{basePath + "rx_bytes"}
	rx, rxErr := strconv.Atoi(execCommand(command, argsRx...))

	if txErr != nil || rxErr != nil {
		return network
	}

	network.tx = tx
	network.rx = rx

	return network
}


func getUserSessions() string {
	command := "bash"
	args := []string{"-c", "w -h | awk '{ printf \"%s %s %s %s \\n\", $1, $3, $4, $6 }'"}
	res := execCommand(command, args...)

	return res
}


func getUserSessionsCount() int {
	command := "bash"
	args := []string{"-c", "w -h | wc -l"}
	res, err := strconv.Atoi(execCommand(command, args...))

	if err != nil {
		log.Fatal(err)
		return -1
	}

	return res
}


func extractFieldFromText(fieldName string, text string) string {
	re := regexp.MustCompile(fieldName + `: ([a-zA-Z0-9_:-]+)`)
	match := re.FindStringSubmatch(text)

	return match[1]
}


func getStatsMessage(validatorChainId string) string {
	return "\n:: SYSTEM ::" + getSystemStatsMessage() +
		"\n\n:: COSMOS ::" + getValidatorStatsMessage(validatorChainId)
}

func getSystemStatsMessage() string {
	system := getSystemStats()

	msg := `
	Date: ` + system.date + `
	Disk: ` + system.disk + `
	Memory: ` + system.mem + `
	Swap: ` + system.swap + `
	CPU: ` + system.cpu + `
	Network: ` + strconv.Itoa(system.network.tx / bytesInAMegabyte) + ` MB tx - ` + strconv.Itoa(system.network.rx / bytesInAMegabyte) + ` MB rx
	Connections: ` + strconv.Itoa(system.users)

	return msg
}

func getValidatorStatsMessage(validatorChainId string) string {
	status := getServiceStatus()
	signing := getValidatorSigning(validatorChainId)
	stats := getValidatorStats()

	msg := `
	Status: ` + status + `
	Missed blocks: ` + signing.missedBlocks + `
	Last Block: ` + stats.lastBlockTime + `
	Network: ` + stats.network + `
	Height: ` + stats.height + `
	Syncing: ` + stats.syncing + `
	Jailed until: ` + signing.jailedUntil + `
	Tombstoned: ` + signing.tombstoned + `
	Voting Power: ` + stats.votingPower

	return msg
}


// func sendMessage(bot *tb.Bot, incomingMessage *tb.Message, outgoingMessage string) {
// 	if incomingMessage.Chat.ID != botChatId {
// 		group := tb.ChatID(botChatId)
// 		bot.Send(group, "Somebody is sending message to the bot!")
// 		return
// 	}

// 	if !incomingMessage.Private() {
// 		return
// 	}

// 	bot.Send(incomingMessage.Sender, outgoingMessage)
// }


func handleGeneralCommands(bot *tb.Bot) {
	validatorChainId = getValidatorNetwork()

	bot.Handle("/hello", func(m *tb.Message) {
		// id := strconv.FormatInt(m.Chat.ID, 10)
		// fmt.Println( id + " - " + m.Chat.Username)
		bot.Send(m.Sender, "Hello, I'm BotScope v2! 🤖 \n\n Type /help to show available commands")
	})

	bot.Handle("/help", func(m *tb.Message) {
		msgCommandList := `
			:: GENERAL ::
			/stats : show all stats

			:: SYSTEM ::
			/sys_stats : show stats
			/sys_sessions : show active user session
			/sys_start : start monitor process
			/sys_stop : stop monitor process
			/sys_data : toggle monitor stats data

			:: FETCH AI ::
			/cosmos_status : show service status
			/cosmos_stats : show stats
			/cosmos_start : start monitor process
			/cosmos_stop : stop monitor process
			/cosmos_data : toggle monitor stats data`

		bot.Send(m.Sender, "Help ❓\n" + msgCommandList)
	})

	bot.Handle("/stats", func(m *tb.Message) {
		bot.Send(m.Sender, "GENERAL :: Service statistics 📊\n" + getStatsMessage(validatorChainId))
	})

	bot.Handle("/kill", func(m *tb.Message) {
		bot.Send(m.Sender, "Killing bot! 💀")
		bot.Stop()
	})
}


func handleSystemCommands(bot *tb.Bot) {
	monitorStarted := false
	monitorInterval := 1800
	monitorShowStats := false
	monitorQuitChannel := make(chan bool)

	var memThreshold float64 = 50
	var swapThreshold float64 = 50
	var diskThreshold float64 = 50
	var cpuThreshold float64 = 50

	validatorChainId = getValidatorNetwork()

	bot.Handle("/sys_stats", func(m *tb.Message) {
		bot.Send(m.Sender, "SYSTEM :: Statistics 📊\n" + getSystemStatsMessage())
	})

	bot.Handle("/sys_sessions", func(m *tb.Message) {
		bot.Send(m.Sender, "SYSTEM :: User Sessions 👥\n\n" + getUserSessions())
	})

	bot.Handle("/sys_start", func(m *tb.Message) {
		bot.Send(m.Sender, "SYSTEM :: Starting monitor 🔎")

		if monitorStarted {
			bot.Send(m.Sender, "SYSTEM :: Monitor already started ⚠")
			return
		}

		system := getSystemStats()
		users := system.users
		tx := system.network.tx
		rx := system.network.rx

		cb := func() {
			if monitorShowStats {
				bot.Send(m.Sender, "SYSTEM :: Statistics 📊\n" + getSystemStatsMessage())
				bot.Send(m.Sender, "SYSTEM :: User Sessions 👥\n\n" + getUserSessions())
			}

			system = getSystemStats()
			currentUsers := system.users
			currentTx := system.network.tx
			currentRx := system.network.rx
			mem, _ := strconv.ParseFloat(strings.Replace(system.mem, "%", "", -1), 8)
			swap, _ := strconv.ParseFloat(strings.Replace(system.swap, "%", "", -1), 8)
			disk, _ := strconv.ParseFloat(strings.Replace(system.disk, "%", "", -1), 8)
			cpu, _ := strconv.ParseFloat(strings.Replace(system.cpu, "%", "", -1), 8)

			if users != currentUsers {
				bot.Send(m.Sender, "SYSTEM :: User sessions opened " + strconv.Itoa(currentUsers) + " - before " + strconv.Itoa(users) )
				bot.Send(m.Sender, "SYSTEM :: User Sessions 👥\n\n" + getUserSessions())
			}

			if mem > memThreshold {
				bot.Send(m.Sender, "SYSTEM :: Memmory level " + system.mem + "! ⚠")
			}

			if swap > swapThreshold {
				bot.Send(m.Sender, "SYSTEM :: Swap level " + system.swap + "! ⚠")
			}

			if disk > diskThreshold {
				bot.Send(m.Sender, "SYSTEM :: Disk level " + system.disk + "! ⚠")
			}

			if cpu > cpuThreshold {
				bot.Send(m.Sender, "SYSTEM :: CPU level " + system.cpu + "! ⚠")
			}

			if tx == currentTx || rx == currentRx {
				bot.Send(m.Sender, "SYSTEM :: Network has no traffic (tx/rx) " + strconv.Itoa(tx) + "/" + strconv.Itoa(rx) + "!!! 🚨")
			}

			users = currentUsers
			tx = currentTx
			rx = currentRx
		}

		monitorStarted = true
		schedule(cb, time.Duration(monitorInterval), monitorQuitChannel)
	})

	bot.Handle("/sys_stop", func(m *tb.Message) {
		monitorQuitChannel <- true
		monitorStarted = false

		bot.Send(m.Sender, "SYSTEM :: Stop monitor! 🛑")
	})

	bot.Handle("/sys_data", func(m *tb.Message) {
		monitorShowStats = !monitorShowStats
		bot.Send(m.Sender, "SYSTEM :: Show monitor stats set to " + strconv.FormatBool(monitorShowStats) + " 📊")
	})
}


func handleCosmosCommands(bot *tb.Bot) {
	debug := false
	monitorStarted := false
	monitorInterval := 1800
	monitorShowStats := false
	monitorQuitChannel := make(chan bool)
	missedBlocksThreshold := 1

	validatorChainId = getValidatorNetwork()

	bot.Handle("/cosmos_status", func(m *tb.Message) {
		command := "systemctl"
		args := []string{"status", serviceName}
		res := execCommand(command, args...)

		bot.Send(m.Sender, "COSMOS :: Service status 💻\n\n" + res)
	})

	bot.Handle("/cosmos_stats", func(m *tb.Message) {
		bot.Send(m.Sender, "COSMOS :: Service statistics 📊\n" + getValidatorStatsMessage(validatorChainId))
	})

	bot.Handle("/cosmos_start", func(m *tb.Message) {
		bot.Send(m.Sender, "COSMOS :: Starting monitor 🔎")

		if monitorStarted {
			bot.Send(m.Sender, "COSMOS :: Monitor already started ⚠")
			return
		}

		missedBlocks := 0

		cb := func() {
			status := getServiceStatus()

			if status != "active" {
				bot.Send(m.Sender, "COSMOS :: Service status is " + status + "!!! 🚨")
			} else {
				signing := getValidatorSigning(validatorChainId)
				currentMissedBlocks, _ := strconv.Atoi(signing.missedBlocks)
				tombstoned, _ := strconv.ParseBool(signing.tombstoned)

				if debug {
					bot.Send(m.Sender, "COSMOS :: Service is active 👌")
				}

				if missedBlocks >= missedBlocksThreshold && currentMissedBlocks > missedBlocks {
					missedBlocks = currentMissedBlocks
					bot.Send(m.Sender, "COSMOS:: Missed blocks increased " + strconv.Itoa(missedBlocks) + "! ⚠")
				}

				if tombstoned {
					bot.Send(m.Sender, "COSMOS :: Tombstoned " + strconv.FormatBool(tombstoned) + "!!! 🚨")
				}
			}

			if monitorShowStats {
				bot.Send(m.Sender, "COSMOS :: Service statistics 📊")
				bot.Send(m.Sender, getValidatorStatsMessage(validatorChainId))
			}
		}

		monitorStarted = true
		schedule(cb, time.Duration(monitorInterval), monitorQuitChannel)
	})

	bot.Handle("/cosmos_stop", func(m *tb.Message) {
		monitorQuitChannel <- true
		monitorStarted = false

		bot.Send(m.Sender, "COSMOS :: Stop monitor! 🛑")
	})

	bot.Handle("/cosmos_debug", func(m *tb.Message) {
		debug = !debug
		bot.Send(m.Sender, "COSMOS :: Debug set to " + strconv.FormatBool(debug) + " 🐞")
	})

	bot.Handle("/cosmos_data", func(m *tb.Message) {
		monitorShowStats = !monitorShowStats
		bot.Send(m.Sender, "COSMOS :: Show monitor stats set to " + strconv.FormatBool(monitorShowStats) + " 📊")
	})
}


func main() {
	poller := &tb.LongPoller{Timeout: 15 * time.Second}

	bot, err := tb.NewBot(tb.Settings{
		Token:  botKey,
		Poller: poller,
	})

	if err != nil {
		log.Fatal(err)
		return
	}

	handleGeneralCommands(bot)
	handleSystemCommands(bot)
	handleCosmosCommands(bot)

	bot.Start()
}